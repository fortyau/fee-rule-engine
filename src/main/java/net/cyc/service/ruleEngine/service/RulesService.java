package net.cyc.service.ruleEngine.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import net.cyc.fee_rules.AccountMaintenanceEvent;
import net.cyc.fee_rules.AtmEvent;
import net.cyc.fee_rules.CheckEvent;
import net.cyc.fee_rules.CopyOfChecksEvent;
import net.cyc.fee_rules.CorrectionOfExcessContributionEvent;
import net.cyc.fee_rules.DebitCardReorderedEvent;
import net.cyc.fee_rules.DepositReturnEvent;
import net.cyc.fee_rules.DormancyEvent;
import net.cyc.fee_rules.InvestmentEvent;
import net.cyc.fee_rules.Message;
import net.cyc.fee_rules.MiscellaneousEvent;
import net.cyc.fee_rules.OverdraftEvent;
import net.cyc.fee_rules.PaperCheckClaimEvent;
import net.cyc.fee_rules.PaperStatementEvent;
import net.cyc.fee_rules.PriorPaperStatementEvent;
import net.cyc.fee_rules.RolloverEvent;
import net.cyc.fee_rules.WireEvent;
import net.cyc.service.ruleEngine.domain.Event;
import net.cyc.service.ruleEngine.domain.enums.EventTypesEnum;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RulesService {

    private final KieContainer kieContainer;
    @Value("${kSessionName}")
    private String kSessionName;

    @Autowired
    public RulesService(KieContainer kieContainer) {
        this.kieContainer = kieContainer;
    }

    public List<Message> runFeeRules(final Event event) {
        if (event == null) {
            return null;
        }

        EventTypesEnum eventTypesEnum = EventTypesEnum.findByType(event.getType());

        KieSession kieSession = kieContainer.newKieSession(kSessionName);

        if (EventTypesEnum.DEBIT_CARD_REORDERED.equals(eventTypesEnum)) {
            DebitCardReorderedEvent ruleEvent = new DebitCardReorderedEvent();
            ruleEvent.setReason(event.getParameters().get(eventTypesEnum.keys()[0]));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.PAPER_CHECK.equals(eventTypesEnum)) {
            PaperCheckClaimEvent ruleEvent = new PaperCheckClaimEvent();
            ruleEvent.setClaimReimbursement(
                Boolean.valueOf(event.getParameters().get(eventTypesEnum.keys()[0])));
            ruleEvent.setMethod(event.getParameters().get(eventTypesEnum.keys()[1]));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.CORRECTION_OF_EXCESS_CONTRIBUTION.equals(eventTypesEnum)) {
            CorrectionOfExcessContributionEvent ruleEvent = new CorrectionOfExcessContributionEvent();
            ruleEvent.setReimbursementRequested(Boolean.valueOf(
                event.getParameters().get(eventTypesEnum.keys()[0])));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.PAPER_STATEMENT.equals(eventTypesEnum)) {
            PaperStatementEvent ruleEvent = new PaperStatementEvent();
            ruleEvent.setEsignCompliant(Boolean.valueOf(
                event.getParameters().get(eventTypesEnum.keys()[0])));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.PRIOR_PAPER_STATEMENT.equals(eventTypesEnum)) {
            PriorPaperStatementEvent ruleEvent = new PriorPaperStatementEvent();
            ruleEvent.setPaperStatementReprint(Boolean.valueOf(
                event.getParameters().get(eventTypesEnum.keys()[0])));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.ACCOUNT_MAINTENANCE.equals(eventTypesEnum)) {
            AccountMaintenanceEvent ruleEvent = new AccountMaintenanceEvent();
            ruleEvent.setAccountStatus(event.getParameters().get(eventTypesEnum.keys()[0]));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.INVESTMENT.equals(eventTypesEnum)) {
            InvestmentEvent ruleEvent = new InvestmentEvent();
            ruleEvent.setInvestmentBalance(Double.valueOf(
                event.getParameters().get(eventTypesEnum.keys()[0])));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.DORMANCY.equals(eventTypesEnum)) {
            DormancyEvent ruleEvent = new DormancyEvent();
            ruleEvent.setAccountStatus(event.getParameters().get(eventTypesEnum.keys()[0]));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.ROLLOVER.equals(eventTypesEnum)) {
            RolloverEvent ruleEvent = new RolloverEvent();
            ruleEvent.setTransactionCode(event.getParameters().get(eventTypesEnum.keys()[0]));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.DEPOSIT_RETURN.equals(eventTypesEnum)) {
            DepositReturnEvent ruleEvent = new DepositReturnEvent();
            ruleEvent.setTransactionCode(event.getParameters().get(eventTypesEnum.keys()[0]));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.COPY_OF_CHECKS.equals(eventTypesEnum)) {
            CopyOfChecksEvent ruleEvent = new CopyOfChecksEvent();
            ruleEvent.setSendCheckImage(Boolean.valueOf(
                event.getParameters().get(eventTypesEnum.keys()[0])));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.OVERDRAFT.equals(eventTypesEnum)) {
            OverdraftEvent ruleEvent = new OverdraftEvent();
            ruleEvent.setSorBalance(Double.valueOf(
                event.getParameters().get(eventTypesEnum.keys()[0])));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.WIRE.equals(eventTypesEnum)) {
            WireEvent ruleEvent = new WireEvent();
            ruleEvent.setWire(Boolean.valueOf(
                event.getParameters().get(eventTypesEnum.keys()[0])));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.ATM.equals(eventTypesEnum)) {
            AtmEvent ruleEvent = new AtmEvent();
            ruleEvent.setAtmUse(Boolean.valueOf(
                event.getParameters().get(eventTypesEnum.keys()[0])));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.CHECK.equals(eventTypesEnum)) {
            CheckEvent ruleEvent = new CheckEvent();
            ruleEvent.setRequestForPaperCheck(Boolean.valueOf(
                event.getParameters().get(eventTypesEnum.keys()[0])));

            kieSession.insert(ruleEvent);
        } else if (EventTypesEnum.MISCELLANEOUS.equals(eventTypesEnum)) {
            MiscellaneousEvent ruleEvent = new MiscellaneousEvent();
            ruleEvent.setProperty(event.getParameters().get(eventTypesEnum.keys()[0]));

            kieSession.insert(ruleEvent);
        }

        kieSession.fireAllRules();
        kieSession.destroy();
        return getMessages(kieSession);
    }

    private ArrayList<Message> getMessages(KieSession kieSession) {
        return new ArrayList<>((Collection<Message>) kieSession.getObjects(object -> object instanceof Message));
    }

    public List<String> findAllFeeEvents() {
        return Arrays.stream(EventTypesEnum.values()).map(EventTypesEnum::type).collect(Collectors.toList());
    }

    public List<String> findAllFeeEventRules() {
        List<String> eventRules = new ArrayList<>();
        kieContainer.newKieSession(kSessionName).getKieBase().getKiePackages().forEach(kiePackage -> kiePackage.getRules().forEach(rule -> eventRules.add(rule.getName())));
        return eventRules;
    }

    public String findFeeEventNameByEngineName(String engineName) {
        Optional<EventTypesEnum> optionalEventTypesEnum = Arrays.stream(EventTypesEnum.values()).filter(eventTypesEnum -> eventTypesEnum.engineName().equals(engineName))
            .findFirst();
        return optionalEventTypesEnum.isPresent() ? optionalEventTypesEnum.get().type() : null;
    }
}
