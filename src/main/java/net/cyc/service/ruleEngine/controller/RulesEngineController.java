package net.cyc.service.ruleEngine.controller;

import java.util.List;
import net.cyc.fee_rules.Message;
import net.cyc.service.ruleEngine.domain.Event;
import net.cyc.service.ruleEngine.service.RulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ruleEngine")
public class RulesEngineController {

    @Autowired
    RulesService rulesService;

    @PostMapping("/runFeeRules")
    public List<Message> runFeeRules(@RequestBody final Event event) {
        return rulesService.runFeeRules(event);
    }

    @GetMapping("/fees/events")
    public List<String> findAllFeeEvents() {
        return rulesService.findAllFeeEvents();
    }

    @GetMapping("/fees/eventsRules")
    public List<String> findAllFeeEventRules() {
        return rulesService.findAllFeeEventRules();
    }

    @GetMapping("/fees/events/engineName/{engineName}")
    public String findFeeEventNameByEngineName(@PathVariable final String engineName) {
        return rulesService.findFeeEventNameByEngineName(engineName);
    }
}
