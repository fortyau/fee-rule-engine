package net.cyc.service.ruleEngine.domain.enums;

public enum EventTypesEnum {

    DEBIT_CARD_REORDERED("Debit card reordered", "debitCardReissued", "reason"),
    PAPER_CHECK("Paper Check", "TODOruleToBeWritten", "Claim Reimbursement", "method"),
    CORRECTION_OF_EXCESS_CONTRIBUTION("Correction of Excess Contribution", "TODOruleToBeWritten", "reimbursementRequested"),
    PAPER_STATEMENT("Paper Statement", "TODOruleToBeWritten", "esignCompliant"),
    PRIOR_PAPER_STATEMENT("Prior Paper Statement", "TODOruleToBeWritten", "paperStatementReprint"),
    ACCOUNT_MAINTENANCE("Account Maintenance", "TODOruleToBeWritten", "accountStatus"),
    INVESTMENT("Investment", "TODOruleToBeWritten", "investmentBalance"),
    DORMANCY("Dormancy", "TODOruleToBeWritten", "accountStatus"),
    ROLLOVER("Rollover", "TODOruleToBeWritten", "transactionCode"),
    DEPOSIT_RETURN("Deposit Return", "TODOruleToBeWritten", "transactionCode"),
    COPY_OF_CHECKS("Copy of Checks", "TODOruleToBeWritten", "sendCheckImage"),
    OVERDRAFT("Overdraft", "TODOruleToBeWritten", "sorBalance"),
    WIRE("Wire", "TODOruleToBeWritten", "wire"),
    ATM("ATM", "TODOruleToBeWritten", "atmUse"),
    CHECK("Check", "TODOruleToBeWritten", "requestForPaperCheck"),
    MISCELLANEOUS("Miscellaneous", "TODOruleToBeWritten", "property");

    private String type;
    private String engineName;
    private String[] keys;

    EventTypesEnum(final String type, final String engineName, final String... keys) {
        this.type = type;
        this.engineName = engineName;
        this.keys = keys;
    }

    public static EventTypesEnum findByType(final String type) {
        for (EventTypesEnum eventTypesEnum : EventTypesEnum.values()) {
            if (eventTypesEnum.type().equalsIgnoreCase(type)) {
                return eventTypesEnum;
            }
        }
        return null;
    }

    public String type() {
        return type;
    }

    public String engineName() {
        return engineName;
    }

    public String[] keys() {
        return keys;
    }
}
