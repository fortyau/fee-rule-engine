package net.cyc.service.ruleEngine.domain;

import java.util.Map;

public class Event {

    private String type;
    private Map<String, String> parameters;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

}
