package net.cyc.service.ruleEngine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuleEngineApplication {

    private static final Logger LOGGER = LoggerFactory
        .getLogger(RuleEngineApplication.class);

    public static void main(String[] args) {
        try {
            SpringApplication.run(RuleEngineApplication.class, args);
        } catch (Exception exception) {
            LOGGER.error("Failed to start application. Exception thrown: ", exception);
            System.exit(1);
        }
    }
}
