package net.cyc.service.ruleEngine.service;

import static org.junit.Assert.assertFalse;

import java.util.HashMap;
import java.util.List;
import net.cyc.fee_rules.Message;
import net.cyc.service.ruleEngine.domain.Event;
import net.cyc.service.ruleEngine.domain.enums.EventTypesEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class RulesServiceTest {

    @Autowired
    RulesService rulesService;

    @Test
    public void runFeeRules() {
        Event event = new Event();
        event.setType(EventTypesEnum.DEBIT_CARD_REORDERED.type());
        event.setParameters(new HashMap<>());
        event.getParameters().put(EventTypesEnum.DEBIT_CARD_REORDERED.keys()[0], "Damaged");

        List<Message> messageList = rulesService.runFeeRules(event);

        assertFalse(CollectionUtils.isEmpty(messageList));
        messageList.forEach(message -> System.out.println(message.getFeeName()));
        messageList.forEach(message -> System.out.println(message.getFeeAmountType()));

    }
}